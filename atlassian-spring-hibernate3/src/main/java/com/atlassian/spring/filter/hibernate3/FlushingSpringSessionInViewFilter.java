package com.atlassian.spring.filter.hibernate3;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.orm.hibernate3.support.OpenSessionInViewFilter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.spring.container.ContainerManager;

/**
 * SessionInViewFilter that explicitly flushes the session if it's connected.
 */
public class FlushingSpringSessionInViewFilter extends OpenSessionInViewFilter {
    private static final Logger log = LoggerFactory.getLogger(FlushingSpringSessionInViewFilter.class);

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (!ContainerManager.isContainerSetup() || !isDatabaseSetUp()) {
            filterChain.doFilter(request, response);
            return;
        }
        super.doFilterInternal(request, response, filterChain);
    }

    protected boolean isDatabaseSetUp() {
        return true;
    }

    // We have to make sure that we throw NO exceptions from this method, or we just end up masking problems
    // in the Spring session filter (the terrible ClobStringType requires active txn sync bug..), and we also
    // interrupt the session cleanup and leak db connections. - cm
    protected void closeSession(Session session, SessionFactory sessionFactory) {
        if (session != null && session.isOpen() && session.isConnected()) {
            try {
                session.flush();
            } catch (Exception e) {
                log.error("Unable to flush Hibernate session. Possible data loss: " + e.getMessage(), e);
            } finally {
                super.closeSession(session, sessionFactory);
            }
        }
    }
}
